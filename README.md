CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Toolbar edit button module adds the node edit and taxonomy term edit
buttons into the admin toolbar.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/toolbar_edit_button

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/toolbar_edit_button


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Toolbar edit button module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

 * After enabling the module, you have to go to "Structure > Block layout > 
   Tabs (block) > Configure" and in "Visibility (vertical tab) > Pages" set 
   " Hide for the listed pages" and for Pages
   /node/*
   /taxonomy/term/*

   This way you would remove the "Local tasks tabs" from the node view (node 
   detail) and taxonomy term view (taxonomy term detail) pages and you will 
   have just the "Edit" button in the admin toolbar (up, right).

   Also, make sure you don't have the "Quick Edit" module activated.


MAINTAINERS
-----------

 * Nikolay Borisov - https://www.drupal.org/u/nikolay-borisov
